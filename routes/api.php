<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    // Authentication middleware is removed

    $api->group(['namespace' => 'App\Api\V1\Controllers'], function (Router $api) {
       /* Station URLs */
       $api->get('stations', 'StationController@all');
       $api->get('stations/{station_id}', 'StationController@item');
       $api->post('stations', 'StationController@save');
       $api->put('stations', 'StationController@update');
       $api->delete('stations/{station_id}', 'StationController@delete');

       /* Route URLs */
       $api->get('routes', 'RouteController@all');
       $api->get('routes/{route_id}', 'RouteController@item');
       $api->post('routes', 'RouteController@save');
       $api->put('routes', 'RouteController@update');
       $api->delete('routes/{route_id}', 'RouteController@delete');

       /* Bus URLs */
       $api->get('buses', 'BusController@all');
       $api->get('buses/{bus_id}', 'BusController@item');
       $api->post('buses', 'BusController@save');
       $api->put('buses', 'BusController@update');
       $api->delete('buses/{bus_id}', 'BusController@delete');

       /* Log URLs */
       $api->get('logs', 'LogController@all');
       $api->get('logs/{log_id}', 'LogController@item');
       $api->post('logs', 'LogController@save');
       $api->put('logs', 'LogController@update');
       $api->delete('logs/{log_id}', 'LogController@delete');
       $api->get('stat', 'LogController@getStatus');

       /* For GSM */
       $api->get('gsm', 'LogController@save');
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
