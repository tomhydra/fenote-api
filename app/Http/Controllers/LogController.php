<?php


namespace App\Http\Controllers;

use App\Api\V1\Requests\LogRequest;
use App\Api\V1\Transformers\LogTransformer;
use App\Http\Controllers\Controller;
use App\Api\V1\Repositories\LogRepository;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class LogController extends Controller {

    use Helpers;
    public $logRepository;

    function __construct(LogRepository $logRepository)
    {
        $this->logRepository = $logRepository;
    }


    public function all()
    {
        $logs = $this->logRepository->all();
        return $this->response->collection($logs, new LogTransformer());
    }


    public function item($logId)
    {
        $log = $this->logRepository->find($logId);
        return $this->response->item($log, new LogTransformer());
    }


    public function save(LogRequest $request)
    {
        $log = $this->logRepository->save($request);
        return $this->response->item($log, new LogTransformer());
    }


    public function update(Request $request)
    {
        $log = $this->logRepository->update($request->log);
        return $this->response->item($log, new LogTransformer());
    }


    public function delete($logId)
    {
        $log = $this->logRepository->delete($logId);
        return $this->response->item($log, new LogTransformer());
    }

    public function messages()
    {
        return [
            'log.code.required' => 'Code is required',
        ];
    }
}