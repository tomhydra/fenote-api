<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\BusRequest;
use App\Api\V1\Transformers\BusTransformer;
use App\Http\Controllers\Controller;
use App\Api\V1\Repositories\BusRepository;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class BusController extends Controller {

    use Helpers;
    public $busRepository;

    function __construct(BusRepository $busRepository)
    {
        $this->busRepository = $busRepository;
    }


    public function all()
    {
        $buses = $this->busRepository->all();
        return $this->response->collection($buses, new BusTransformer());
    }


    public function item($busId)
    {
        $bus = $this->busRepository->find($busId);
        return $this->response->item($bus, new BusTransformer());
    }


    public function save(BusRequest $request)
    {
        $bus = $this->busRepository->save($request);
        return $this->response->item($bus, new BusTransformer());
    }


    public function update(Request $request)
    {
        $bus = $this->busRepository->update($request);
        return $this->response->item($bus, new BusTransformer());
    }


    public function delete($busId)
    {
        $bus = $this->busRepository->delete($busId);
        return $this->response->item($bus, new BusTransformer());
    }
}