<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\RouteRequest;
use App\Api\V1\Transformers\RouteTransformer;
use App\Http\Controllers\Controller;
use App\Api\V1\Repositories\RouteRepository;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class RouteController extends Controller {

    use Helpers;
    public $routeRepository;

    function __construct(RouteRepository $routeRepository)
    {
        $this->routeRepository = $routeRepository;
    }


    public function all()
    {
        $routes = $this->routeRepository->all();
        return $this->response->collection($routes, new RouteTransformer());
    }


    public function item($routeId)
    {
        $route = $this->routeRepository->find($routeId);
        return $this->response->item($route, new RouteTransformer());
    }


    public function save(RouteRequest $request)
    {
        $route = $this->routeRepository->save($request);
        return $this->response->item($route, new RouteTransformer());
    }


    public function update(Request $request)
    {
        $route = $this->routeRepository->update($request);
        return $this->response->item($route, new RouteTransformer());
    }


    public function delete($routeId)
    {
        $route = $this->routeRepository->delete($routeId);
        return $this->response->item($route, new RouteTransformer());
    }
}