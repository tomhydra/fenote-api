<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\StationRequest;
use App\Api\V1\Transformers\StationTransformer;
use App\Http\Controllers\Controller;
use App\Api\V1\Repositories\StationRepository;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class StationController extends Controller {

    use Helpers;
    public $stationRepository;

    function __construct(StationRepository $stationRepository)
    {
        $this->stationRepository = $stationRepository;
    }


    public function all()
    {
        $stations = $this->stationRepository->all();
        return $this->response->collection($stations, new StationTransformer());
    }


    public function item($stationId)
    {
        $station = $this->stationRepository->find($stationId);
        return $this->response->item($station, new StationTransformer());
    }


    public function save(StationRequest $request)
    {
        $station = $this->stationRepository->save($request);
        return $this->response->item($station, new StationTransformer());
    }


    public function update(Request $request)
    {
        $station = $this->stationRepository->update($request);
        return $this->response->item($station, new StationTransformer());
    }


    public function delete($stationId)
    {
        $station = $this->stationRepository->delete($stationId);
        return $this->response->item($station, new StationTransformer());
    }
}