<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\StationRouteTransformer;
use App\Http\Controllers\Controller;
use App\Api\V1\Repositories\StationRouteRepository;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class StationRouteController extends Controller {

    use Helpers;
    public $stationRouteRepository;

    function __construct(StationRouteRepository $stationRouteRepository)
    {
        $this->stationRouteRepository = $stationRouteRepository;
    }


    public function all()
    {
        $stationRoutes = $this->stationRouteRepository->all();
        return $this->response->collection($stationRoutes, new StationRouteTransformer());
    }


    public function item($stationRouteId)
    {
        $stationRoute = $this->stationRouteRepository->find($stationRouteId);
        return $this->response->item($stationRoute, new StationRouteTransformer());
    }


    public function save(Request $request)
    {
        $stationRoute = $this->stationRouteRepository->save($request->stationRoute);
        return $this->response->item($stationRoute, new StationRouteTransformer());
    }


    public function update(Request $request)
    {
        $stationRoute = $this->stationRouteRepository->update($request->stationRoute);
        return $this->response->item($stationRoute, new StationRouteTransformer());
    }


    public function delete($stationRouteId)
    {
        $stationRoute = $this->stationRouteRepository->delete($stationRouteId);
        return $this->response->item($stationRoute, new StationRouteTransformer());
    }
}