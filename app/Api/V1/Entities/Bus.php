<?php

namespace App\Api\V1\Entities;


use Illuminate\Database\Eloquent\Model;

class Bus extends Model {

    public $table = 'bus';

    public function route()
    {
        return $this->belongsTo(Route::class, 'route_id');
    }

    public function logs()
    {
        return $this->hasMany(Log::class, 'bus_id');
    }

}