<?php

namespace App\Api\V1\Entities;


use Illuminate\Database\Eloquent\Model;

class Log extends Model {

    public $table = 'log';

    public function station()
    {
        return $this->belongsTo(Station::class, 'station_id');
    }

    public function bus()
    {
        return $this->belongsTo(Bus::class, 'bus_id');
    }

    public function next()
    {
        return $this->belongsTo(Station::class, 'next');
    }
}