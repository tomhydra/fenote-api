<?php

namespace App\Api\V1\Entities;


use Illuminate\Database\Eloquent\Model;

class Route extends Model {

    public $table = 'routes';

    public function buses()
    {
        return $this->hasMany(Bus::class, 'route_id');
    }

    public function stations()
    {
        return $this->belongsToMany(Station::class, 'route_stations')->withPivot('order')->withTimestamps();
    }

}