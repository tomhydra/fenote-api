<?php

namespace App\Api\V1\Entities;


use Illuminate\Database\Eloquent\Model;

class StationRoute extends Model {

    public $table = 'station_routes';

    public function station()
    {
        return $this->belongsTo(Station::class, 'station_id');
    }

    public function route()
    {
        return $this->belongsTo(Station::class, 'route_id');
    }

    public function next_station()
    {
        return $this->belongsTo(Station::class, 'station_id');
    }

    public function prev_station()
    {
        return $this->belongsTo(Station::class, 'station_id');
    }
}