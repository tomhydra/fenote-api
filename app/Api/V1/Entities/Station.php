<?php

namespace App\Api\V1\Entities;


use Illuminate\Database\Eloquent\Model;

class Station extends Model {

    public $table = 'stations';

    function routes() {
        return $this->belongsToMany(Route::class, 'route_stations')->withPivot('order')->withTimestamps();
    }

}