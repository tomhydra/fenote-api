<?php

namespace App\Api\V1\Repositories;


use App\Api\V1\Entities\Bus;
use App\Api\V1\Entities\Log;
use App\Api\V1\Entities\Route;
use App\Api\V1\Entities\Station;
use Mockery\Exception;

class LogRepository {

    public function all()
    {
        return Log::get();
    }


    public function find($logId)
    {
        return Log::find($logId);
    }

    public function save($input)
    {
        $log = new Log();
        $log->station_id = $this->getStation($input['ss'])->id;
        $log->bus_id = $this->getBus($input['cd'])->id;
        $log->passengers = $input['ps'];
        $log->current_time = $input['ct'];
        $log->arrival_time = $input['at'];
        $log->status = $input['st'];
//        $log->next = $log->station_id; // To be removed
        $log->save();
        return $log;

    }


    public function update($input)
    {
        $log = Log::find($input['id']);
        $log->station_id = isset($input['station_id']) ? $input['station_id'] : $log->station_id;
        $log->bus_id = isset($input['bus_id']) ? $input['bus_id'] : $log->bus_id;
        $log->passengers = isset($input['passengers']) ? $input['passengers'] : $log->passengers;
        $log->current_time = isset($input['current_time']) ? $input['current_time'] : $log->current_time;
        $log->arrival_time = isset($input['arrival_time']) ? $input['arrival_time'] : $log->arrival_time;
        $log->status = isset($input['status']) ? $input['status'] : $log->status;
        $log->save();
        return $log;
    }


    public function delete($logId)
    {
        $log = Log::find($logId);
        $log->delete();
        return $log;
    }

    public function getNext($station, $route)
    {
        $stations = $route->stations()->sortBy('pivot.order');
        dd($stations);
        foreach ($stations as $st) {

        }
    }

    public function getBus($code)
    {
        return Bus::where('code', $code)->first();
    }

    public function getStation($station)
    {
        return Station::where('name', $station)->first();
    }

    public function getStatus()
    {
        $bus_count = Bus::get()->count();
        $station_count = Station::get()->count();
        $route_count = Route::get()->count();
        $buses = Bus::get();
        $damages = Log::where('status', 'accident')->count();
        $busChar = [];
        $passengers = Log::get()->sum('passengers');
        foreach ($buses as $bus) {
            $busChar[] = ["name" => "Bus " . $bus->code, "y" => Log::where('bus_id', $bus->id)->get()->sum('passengers')];
        }
        return [
            "buses" => $bus_count,
            "stations" => $station_count,
            "routes" => $route_count,
            "busChart" => $busChar,
            "passengers" => $passengers,
            "damages" => $damages
        ];
    }
}