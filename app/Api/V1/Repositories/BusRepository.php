<?php

namespace App\Api\V1\Repositories;


use App\Api\V1\Entities\Bus;

class BusRepository {

    public function all()
    {
        return Bus::get();
    }


    public function find($busId)
    {
        return Bus::find($busId);
    }

    public function save($input)
    {
        $bus = new Bus();
        $bus->code = $input['code'];
        $bus->route_id = $input['route_id'];
        $bus->save();
        return $bus;

    }


    public function update($input)
    {
        $bus = Bus::find($input['id']);
        $bus->code = isset($input['code']) ? $input['code'] : $bus->code;
        $bus->route_id = isset($input['route_id']) ? $input['route_id'] : $bus->route_id;
        $bus->save();
        return $bus;
    }


    public function delete($busId)
    {
        $bus = Bus::find($busId);
        $bus->delete();
        return $bus;
    }

    public function search()
    {



    }
}