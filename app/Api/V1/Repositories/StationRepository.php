<?php

namespace App\Api\V1\Repositories;


use App\Api\V1\Entities\Station;

class StationRepository {

    public function all()
    {
        return Station::get();
    }


    public function find($stationId)
    {
        return Station::find($stationId);
    }

    public function save($input)
    {
        $station = new Station();
        $station->name = $input['name'];
        $station->save();
        return $station;

    }


    public function update($input)
    {
        $station = Station::find($input['id']);
        $station->name = isset($input['name']) ? $input['name'] : $station->name;
        $station->save();
        return $station;
    }


    public function delete($stationId)
    {
        $station = Station::find($stationId);
        $station->delete();
        return $station;
    }
}