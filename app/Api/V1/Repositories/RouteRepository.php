<?php

namespace App\Api\V1\Repositories;


use App\Api\V1\Entities\Route;
use App\Api\V1\Entities\Station;
use Illuminate\Database\QueryException;

class RouteRepository {

    public function all()
    {
        return Route::get();
    }


    public function find($routeId)
    {
        return Route::find($routeId);
    }

    public function save($input)
    {
        try {
            $route = new Route();
            $route->name = $input['name'];
            $route->save();
            $stationIds = $input['stations'];
            for ($i = 0; $i < count($stationIds); $i++) {
                $route->stations()->attach($stationIds[$i], ['order'=> $i + 1]);
            }
        } catch(QueryException $e) {
            if ($e->errorInfo[1] == 1062){
                throw new \Exception('Name must be unique');
            } else {
                throw new \Exception('Error in your input');
            }
        }
        return $route;

    }


    public function update($input)
    {
        $route = Route::find($input['id']);
        $route->start_location = isset($input['start_location']) ? $input['start_location'] : $route->start_location;
        $route->end_location = isset($input['end_location']) ? $input['end_location'] : $route->end_location;
        $route->save();
        return $route;
    }


    public function delete($routeId)
    {
        $route = Route::find($routeId);
        $route->delete();
        return $route;
    }
}