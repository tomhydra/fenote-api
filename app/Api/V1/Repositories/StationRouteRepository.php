<?php

namespace App\Api\V1\Repositories;


use App\Api\V1\Entities\StationRoute;

class StationRouteRepository {

    public function all()
    {
        return StationRoute::get();
    }


    public function find($stationRouteId)
    {
        return StationRoute::find($stationRouteId);
    }

    public function save($input)
    {
        $stationRoute = new StationRoute();
        $stationRoute->station_id = $input['station_id'];
        $stationRoute->route_id = $input['route_id'];
        $stationRoute->next_station_id = $input['next_station_id'];
        $stationRoute->prev_station_id = $input['prev_station_id'];
        $stationRoute->save();
        return $stationRoute;

    }


    public function update($input)
    {
        $stationRoute = StationRoute::find($input['id']);
        $stationRoute->station_id = isset($input['station_id']) ? $input['station_id'] : $stationRoute->station_id;
        $stationRoute->route_id = isset($input['route_id']) ? $input['route_id'] : $stationRoute->route_id;
        $stationRoute->next_station_id = isset($input['next_station_id']) ? $input['next_station_id'] : $stationRoute->next_station_id;
        $stationRoute->prev_station_id = isset($input['prev_station_id']) ? $input['prev_station_id'] : $stationRoute->prev_station_id;
        $stationRoute->save();
        return $stationRoute;
    }


    public function delete($stationRouteId)
    {
        $stationRoute = StationRoute::find($stationRouteId);
        $stationRoute->delete();
        return $stationRoute;
    }
}