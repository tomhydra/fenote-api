<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\V1\Entities\Station;

class StationTransformer extends TransformerAbstract
{

    public function transform(Station $station)
    {
        return [
            'id' => $station->id,
            'name' => $station->name,
        ];
    }
}