<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\V1\Entities\Log;

class LogTransformer extends TransformerAbstract {

    protected $defaultIncludes = [];
    protected $availableIncludes = ['station', 'bus'];

    public function transform(Log $log)
    {
        return [
            "id" => $log->id,
            "time" => $log->current_time,
            "arrival_time" => $log->arrival_time,
            "status" => $log->status,
            "bus_code" => $log->bus->code,
            "route_name" => $log->bus->route->name,
            "station" => $log->station->name,
            "passengers" => $log->passengers,
        ];
    }

    public function includeStation(Log $log) {
        return $this->item($log->station()->first(), new StationTransformer());
    }

    public function includeBus(Log $log) {
        return $this->item($log->bus()->first(), new BusTransformer());
    }
}