<?php

namespace App\Api\V1\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Api\V1\Entities\Log;

class CustomLogTransformer extends TransformerAbstract {

    protected $defaultIncludes = [];
    protected $availableIncludes = ['station', 'bus'];

    public function transform(Log $log)
    {
        return [
            "time" => $log->current_time,
            "status" => $log->status,
            "passengers" => $log->passengers,
            "station" => $log->station->name,
            "created_at" => $log->created_at ? Carbon::instance($log->created_at)->toDateTimeString() : null,
            "current_log" => $log->last,
        ];
    }

    public function includeStation(Log $log) {
        return $this->item($log->station()->first(), new StationTransformer());
    }

    public function includeBus(Log $log) {
        return $this->item($log->bus()->first(), new BusTransformer());
    }
}