<?php

namespace App\Api\V1\Transformers;

use App\Api\V1\Entities\Bus;
use App\Api\V1\Entities\Log;
use App\Api\V1\Entities\Station;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;
use Mockery\Exception;

class BusTransformer extends TransformerAbstract {

    protected $availableIncludes = ['logs', 'route'];
//    protected $defaultIncludes = ['logs'];

    public function transform(Bus $bus)
    {
        return [
            "id" => $bus->id,
            "code" => $bus->code,
            "route_id" => $bus->route->id,
            "route_name" => $bus->route->name,
        ];
    }

    public function includeRoute(Bus $bus)
    {
        return $this->item($bus->route, new RouteTransformer());
    }

    public function includeLogs(Bus $bus) {
        $stations = $bus->route->stations()->get()->sortBy('pivot.order');
        $station_ids = $stations->map(function ($item){
            return $item["id"];
        });
        if (!is_array($station_ids)){
            $station_ids = $station_ids->toArray();
        }
        $custom_logs = new Collection();
        foreach ($station_ids as $station_id) {
            $new_log = new Log();
            $new_log->station_id = $station_id;
            $new_log->status = 'unknown';
            $custom_logs->push($new_log);
        }

        $logs = $bus->logs()->whereIn('station_id', $station_ids)->get()->sortByDesc('created_at')->unique('station_id')->sortBy('created_at');
        $logs_copy = new Collection();
        foreach ($logs as $log) {
            $logs_copy->push($log);
        }
        $count = count($logs);
        if ($count){   // There is atleast on record
            $first = $logs->pop();
            $current_pos = array_search($first->station_id, $station_ids) + 1;  // Order of the station in the current route
            if ($current_pos == 1){
                $next_pos = $current_pos + 1; // Take the next station if order is 1
            } else if ($current_pos == count($station_ids)) {
                $next_pos = $current_pos - 1; // Take the previous station if order is last
            } else {
                if ($count == 1) { // If there is only one record
                    $next_pos = $current_pos + 1; // Take the next station by default
                } else { // Else if there are more than one stations
                    $second = $logs->pop();
                    $pos2 = array_search($second->station_id, $station_ids) + 1; // Order of the second station in the current route
                    if ($current_pos > $pos2){ // Moving forward or 1 -> 2 -> 3 -> ...
                        $next_pos = $current_pos + 1;
                    } else {  // Moving backward or ... -> 5 -> 4 -> 3 -> ... -> 1
                        $next_pos = $current_pos - 1;
                    }
                }
            }
            $custom_logs = $this->setNextStations($next_pos, $current_pos, $first, $custom_logs, $logs_copy);
        }
        return $this->collection($custom_logs, new CustomLogTransformer());
    }

    public function setNextStations($next_pos, $current_pos, $current_log, $custom_logs, $logs)
    {
        if ($next_pos > $current_pos) {
            $temp = $next_pos;
            while ($temp <= count($custom_logs)) {
                $custom_logs[$temp - 1]->current_time = $temp == $next_pos ? $current_log->arrival_time : null;
                $custom_logs[$temp - 1]->passengers = $current_log->passengers;
                $custom_logs[$temp - 1]->status = $current_log->status == 'active' ? 'coming' : 'unknown';
                $temp++;
            }
            while ($current_pos >= 1) {
                $c_log = $logs->where('station_id', $custom_logs[$current_pos - 1]->station_id)->first();
                if ($c_log){
                    $custom_logs[$current_pos - 1]->current_time = $c_log->current_time;
                    $custom_logs[$current_pos - 1]->passengers = $c_log->passengers;
                    $custom_logs[$current_pos - 1]->status = $c_log->status;
                    $custom_logs[$current_pos - 1]->created_at = $c_log->created_at;
                    $custom_logs[$current_pos - 1]->last = $c_log->station_id == $current_log->station_id;
                }
                $current_pos--;
            }
        } else {
            $temp = $next_pos;
            while ($temp >= 1) {
                $custom_logs[$temp - 1]->current_time = $temp == $next_pos ? $current_log->arrival_time : null;
                $custom_logs[$temp - 1]->passengers = $current_log->passengers;
                $custom_logs[$temp - 1]->status = $current_log->status == 'active' ? 'coming' : 'unknown';
                $temp--;
            }
            while ($current_pos <= count($custom_logs)) {
                $c_log = $logs->where('station_id', $custom_logs[$current_pos - 1]->station_id)->first();
                if ($c_log) {
                    $custom_logs[$current_pos - 1]->current_time = $c_log->current_time;
                    $custom_logs[$current_pos - 1]->passengers = $c_log->passengers;
                    $custom_logs[$current_pos - 1]->status = $c_log->status;
                    $custom_logs[$current_pos - 1]->created_at = $c_log->created_at;
                    $custom_logs[$current_pos - 1]->last = $c_log->station_id == $current_log->station_id;
                }
                $current_pos++;
            }
        }
        return $custom_logs;
    }
}