<?php

namespace App\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\V1\Entities\Route;

class RouteTransformer extends TransformerAbstract {

    protected $defaultIncludes = ['stations'];

    public function transform(Route $route)
    {
        return [
            "id" => $route->id,
            "name" => $route->name,
        ];
    }

    public function includeStations(Route $route) {
        $stations = $route->stations()->get();
        return $this->collection($stations, new RouteStationTransformer());
    }
}