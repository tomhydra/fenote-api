<?php

namespace App\Api\V1\Requests;


use Dingo\Api\Http\FormRequest;

class LogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cd' => 'required|exists:bus,code|max:5',
            'ss' => 'required|exists:stations,name',
            "ps" => 'required',
            'ct' => 'required',
            'at' => 'required',
            'st' => 'required|in:active,accident,break,stop',
        ];
    }




}
