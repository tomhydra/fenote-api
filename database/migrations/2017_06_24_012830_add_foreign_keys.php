<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bus', function (Blueprint $table) {
            $table->integer('route_id')->unsigned()->change(); // Setting route_id to unsigned to match the primary key signature
           $table->foreign('route_id')->references('id')->on('routes');
        });

        Schema::table('route_stations', function (Blueprint $table) {
            $table->integer('route_id')->unsigned()->change();
           $table->foreign('route_id')
               ->references('id')
               ->on('routes')->onDelete('cascade');
            $table->integer('station_id')->unsigned()->change();
           $table->foreign('station_id')
               ->references('id')
               ->on('stations')->onDelete('cascade');
        });

        Schema::table('location_tracker', function (Blueprint $table) {
            $table->integer('station_id')->unsigned()->change();
           $table->foreign('station_id')
               ->references('id')
               ->on('stations')->onDelete('cascade');
            $table->integer('bus_id')->unsigned()->change();
           $table->foreign('bus_id')
               ->references('id')
               ->on('bus')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropForeign('bus_route_id_foriegn');
        Schema::dropForeign('route_stations_route_id_foriegn');
        Schema::dropForeign('route_stations_station_id_foriegn');
        Schema::dropForeign('location_tracker_station_id_foriegn');
        Schema::dropForeign('location_tracker_bus_id_foriegn');
    }
}
